/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.19 : Database - wms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `wms`;

/*Table structure for table `goods` */

DROP TABLE IF EXISTS `goods`;

CREATE TABLE `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '货名',
  `storage` int(11) NOT NULL COMMENT '仓库',
  `goodsType` int(11) NOT NULL COMMENT '分类',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `goods` */

insert  into `goods`(`id`,`name`,`storage`,`goodsType`,`count`,`remark`) values 
(1,'篮球',1,1,9,''),
(2,'乒乓球',1,2,100,'');

/*Table structure for table `goodstype` */

DROP TABLE IF EXISTS `goodstype`;

CREATE TABLE `goodstype` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '分类名',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `goodstype` */

insert  into `goodstype`(`id`,`name`,`remark`) values 
(1,'食品类','存放食品'),
(2,'文体类','存放文体物品');

/*Table structure for table `memu` */

DROP TABLE IF EXISTS `memu`;

CREATE TABLE `memu` (
  `id` int(11) NOT NULL,
  `memuCode` varchar(8) DEFAULT NULL COMMENT '菜单编码',
  `memuName` varchar(16) DEFAULT NULL COMMENT '菜单名字',
  `memuLevel` varchar(2) DEFAULT NULL,
  `memuParentCode` varchar(12) DEFAULT NULL,
  `memuRight` varchar(16) DEFAULT NULL,
  `memuComponent` varchar(200) DEFAULT NULL,
  `memuIcon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memu` */

insert  into `memu`(`id`,`memuCode`,`memuName`,`memuLevel`,`memuParentCode`,`memuRight`,`memuComponent`,`memuIcon`) values 
(1,'管理员管理','001','1','Admin','0','admin/AdminManage.vue','el-icon-s-custom'),
(2,'用户管理','002','1','User','0,1','user/UserManage.vue','el-icon-user-solid'),
(3,'仓库管理','003','1','Storage','0,1','storage/StorageManage.vue','el-icon-office-building'),
(4,'物品分类管理','004','1','Goodstype','0,1','goodstype/GoodstypeManage.vue','el-icon-menu'),
(5,'物品管理','005','1','Goods','0,1,2','goods/GoodsManage.vue','el-icon-s-management'),
(6,'记录管理','006','1','Record','0,1,2','record/RecordManage.vue','el-icon-s-order');

/*Table structure for table `record` */

DROP TABLE IF EXISTS `record`;

CREATE TABLE `record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods` int(11) NOT NULL COMMENT '货品id',
  `userId` int(11) DEFAULT NULL COMMENT '取货人/补货人',
  `admin_id` int(11) DEFAULT NULL COMMENT '操作人id',
  `count` int(11) DEFAULT NULL COMMENT '数量',
  `createtime` datetime DEFAULT NULL COMMENT '操作时间',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `record` */

insert  into `record`(`id`,`goods`,`userId`,`admin_id`,`count`,`createtime`,`remark`) values 
(4,1,9,8,-1,NULL,'1');

/*Table structure for table `storage` */

DROP TABLE IF EXISTS `storage`;

CREATE TABLE `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '仓库名',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `storage` */

insert  into `storage`(`id`,`name`,`remark`) values 
(1,'仓库1','这是一个小仓库'),
(2,'仓库2','二号仓库');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `no` varchar(20) DEFAULT NULL COMMENT '账号',
  `name` varchar(100) NOT NULL COMMENT '名字',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `age` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL COMMENT '性别',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `role_id` int(11) DEFAULT NULL COMMENT '⻆⾊ 0超级管理员，1管理员，2普通账号',
  `isValid` varchar(4) DEFAULT 'Y' COMMENT '是否有效，Y有效，其他⽆效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`no`,`name`,`password`,`age`,`sex`,`phone`,`role_id`,`isValid`) values 
(1,'user','用户','123456',19,1,'15929258137',1,'Y'),
(2,'admin','管理员','123456',22,1,'15191074177',0,'Y');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

# 前后端分离项目 - 仓库管理系统，Vue+spring boot

#### 项目介绍
前后端分离仓库管理系统，前端用vue+elementUI、后端用springboot+mybatisPlus、数据库mysql，非常好的毕设学习项目

#### 项目技术架构
后端：Springboot  mybatis-plus  java
前端：nodejs  vue脚手架  element-ui
数据库：mysql

#### 功能介绍

由于是一个非常简单的项目，所以功能方面毕竟简单，主要有管理员管理、用户管理、仓库管理、物品分类管理、出库入库、物品管理。以及记录管理。

#### 搭建教程

1.后端依赖从meaven中拉取，拉取完毕在application.yml中修改数据库信息，导入sql文件到mysql，启动后端项目。

2.前端项目先使用 npm install 安装依赖，然后使用 npm run serve 启动项目。

#### 账号密码

管理员 admin 123456

用户 user 123456

#### 项目展示

首页

![](https://tu.zxiyun.com/local/2024/02/08/65c4ca8d33457.png)

用户管理页面

![](https://tu.zxiyun.com/local/2024/02/08/65c4ca8d2f694.png)

仓库管理页面

![](https://tu.zxiyun.com/local/2024/02/08/65c4ca8d0d2c0.png)

物品管理页面

![](https://tu.zxiyun.com/local/2024/02/08/65c4ca8d1267d.png)

记录管理页面

![](https://tu.zxiyun.com/local/2024/02/08/65c4ca8d277db.png)

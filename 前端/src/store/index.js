import Vue from "vue";
import Vuex from "vuex";
import router, { resetRouter } from "@/router";
Vue.use(Vuex)

// 添加新的路由
function addNewRoute(menuList) {
    let routes = router.options.routes
    routes.forEach(routeItem => {
        if (routeItem.path == '/index') {
            menuList.forEach(menu => {
                let childRoute = {
                    path:'/'+menu.memuparentcode,
                    name:menu.memucode,
                    meta:{
                        title:menu.memucode
                    },
                    component:()=>import('../components/'+menu.memucomponent)
                }

                routeItem.children.push(childRoute)
            })
        }
    })

    //路由制空
    resetRouter()
    router.addRoutes(routes)
}



export default new Vuex.Store({
    state: {
        menu:[]
    },
    mutations: {
        setMenu(state,menuList) {
            state.menu = menuList

            addNewRoute(menuList)
        }
    },
    actions: {
        
    },
    getters: {
        getMenu(state) {
            return state.menu
        }
    }
})



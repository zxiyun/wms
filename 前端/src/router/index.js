
import VueRouter from "vue-router";


const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import('../components/Login'),
    },
    {
        path: '/index',
        name: 'index',
        component: () => import('../components/Index'),
        children: [
            {
                path: '/home',
                name: 'home',
                meta: {
                    title:'首页'
                },
                component: () => import('../components/Home'),
            },
        ]
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

// 重置路由,解决动态添加路由时候，路由重复的问题，先调用函数把路由制空，然后再全部添加就不会重复了
export function resetRouter() {
    router.matcher = new VueRouter({
        mode:'history',
        routes: []
    }).matcher
}

const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
    return VueRouterPush.call(this, to).catch(err => err)
}
export default router;
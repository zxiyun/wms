package com.wms.mapper;

import com.wms.entity.Memu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author miaoyan
 * @since 2023-03-10
 */
@Mapper
public interface MemuMapper extends BaseMapper<Memu> {

}

package com.wms.service;

import com.wms.entity.Memu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author miaoyan
 * @since 2023-03-10
 */
public interface MemuService extends IService<Memu> {

}

package com.wms.service.impl;

import com.wms.entity.Memu;
import com.wms.mapper.MemuMapper;
import com.wms.service.MemuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author miaoyan
 * @since 2023-03-10
 */
@Service
public class MemuServiceImpl extends ServiceImpl<MemuMapper, Memu> implements MemuService {

}

package com.wms.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wms.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wms.mapper.UserMapper;

import javax.annotation.Resource;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author miaoyan
 * @since 2023-03-10
 */
public interface UserService extends IService<User> {
    IPage pageC(IPage<User> page);

    IPage pageCC(IPage<User> page, Wrapper wrapper);
}

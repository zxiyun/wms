package com.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author miaoyan
 * @since 2023-03-10
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("memu")
public class Memu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 菜单编码
     */
    private String memucode;

    /**
     * 菜单名字
     */
    private String memuname;

    private String memulevel;

    private String memuparentcode;

    private String memuright;

    private String memucomponent;

    private String memuicon;


}

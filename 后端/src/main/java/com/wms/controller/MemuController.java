package com.wms.controller;


import com.wms.common.Result;
import com.wms.entity.Memu;
import com.wms.service.MemuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author miaoyan
 * @since 2023-03-10
 */
@RestController
@RequestMapping("/memu")
public class MemuController {
    @Autowired
    private MemuService menuService;
    @GetMapping("/list")
    public Result list(@RequestParam String roleId){
        List list = menuService.lambdaQuery().like(Memu::getMemuright,roleId).list();
        return Result.suc(list);
    }
}

